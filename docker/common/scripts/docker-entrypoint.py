#!/usr/bin/env python
import subprocess
import sys
from os import environ as env, execvp

from await_postgres_connection import await_postgres_connection


def execute(program: str, *args: str):
    execvp(program, [program, *args])


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, flush=True, **kwargs)


def required(variable_name: str, require_non_empty: bool = True):
    if variable_name not in env:
        eprint(f'The variable \'{variable_name}\' has not been supplied.')
        sys.exit(1)

    if require_non_empty and not env[variable_name]:
        eprint(f'The variable \'{variable_name}\' may not be empty.')
        sys.exit(1)


def export_secret(secret_variable_name: str, require_non_empty: bool = True):
    secret_file_variable_name = f'{secret_variable_name}_FILE'

    if secret_variable_name in env and secret_file_variable_name in env:
        eprint(
            f'Either the secret file \'{secret_file_variable_name}\''
            f' or the secret itself \'{secret_variable_name}\','
            ' not both, should be supplied.')
        sys.exit(1)
    elif secret_file_variable_name in env:
        secret_file = env[secret_file_variable_name]
        try:
            with open(secret_file, 'r') as file:
                env[secret_variable_name] = file.read()
        except OSError:
            eprint(
                f'The secret file \'{secret_file}\''
                f' is not a readable regular file.')
            sys.exit(1)
    elif secret_variable_name not in env:
        eprint(
            f'Either a secret file supplied by \'{secret_file_variable_name}\''
            f' or the secret itself supplied by \'{secret_variable_name}\''
            ' has to be specified.')
        sys.exit(1)

    required(secret_variable_name, require_non_empty)


def django_manage(*args: str) -> bool:
    return subprocess.run(['python', 'manage.py', *args]).returncode == 0


def django_migrate() -> bool:
    return django_manage('migrate', '--no-input')


def django_collect_static() -> bool:
    return django_manage('collectstatic', '--no-input', '--clear')


def django_compilemessages() -> bool:
    return django_manage('compilemessages')


POSTGRES_HOST_VARIABLE_NAME = 'POSTGRES_HOST'
POSTGRES_DB_VARIABLE_NAME = 'POSTGRES_DB'
POSTGRES_USER_VARIABLE_NAME = 'POSTGRES_USER'

REQUIRED_VARIABLES = [
    POSTGRES_HOST_VARIABLE_NAME,
    'BASE_URL',
]
REQUIRED_SECRET_VARIABLES = [
    POSTGRES_DB_VARIABLE_NAME,
    POSTGRES_USER_VARIABLE_NAME,
    'POSTGRES_PASSWORD',
    'DJANGO_SECRET_KEY',
    'RECORDINGS_SECRET',
]

for secret_variable in REQUIRED_VARIABLES:
    required(secret_variable)
for secret_variable in REQUIRED_SECRET_VARIABLES:
    export_secret(secret_variable)


def prepare_django():
    if not await_postgres_connection(env[POSTGRES_HOST_VARIABLE_NAME],
                                     env[POSTGRES_DB_VARIABLE_NAME],
                                     env[POSTGRES_USER_VARIABLE_NAME]):
        eprint('Could not connect to the database.')
        sys.exit(1)

    if not django_migrate():
        eprint('Could not migrate database.')
        sys.exit(1)

    if not django_collect_static():
        eprint('Could not collect static files.')
        sys.exit(1)

    if not django_compilemessages():
        eprint('Could not compile locale files.')
        sys.exit(1)


if len(sys.argv) == 1:
    prepare_django()
    execute('gunicorn', 'BBBatScale.wsgi', '--config', 'gunicorn-init.py')

elif sys.argv[1] == "runserver":
    prepare_django()
    execute('python', 'manage.py', *sys.argv[1:])

else:
    execute(*sys.argv[1:])
