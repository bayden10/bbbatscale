import os

os.environ.setdefault('DJANGO_SECRET_KEY', 'development_django_secret_key')
os.environ.setdefault('BASE_URL', 'localhost:8000')
os.environ.setdefault('RECORDINGS_SECRET', 'development_recordings_secret')
os.environ.setdefault('POSTGRES_DB', 'development_postgres_database')
os.environ.setdefault('POSTGRES_USER', 'development_postgres_user')
os.environ.setdefault('POSTGRES_PASSWORD', 'development_postgres_password')
os.environ.setdefault('POSTGRES_HOST', 'localhost')

from .base import *  # noqa: F401,F403,E402

DEBUG = True
