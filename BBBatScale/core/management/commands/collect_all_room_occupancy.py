# encoding: utf-8
import logging
from django.core.management.base import BaseCommand
from core.models import Room
from core.services import collect_room_occupancy

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'collect events for rooms according to the strategies associated with rooms'

    def handle(self, *args, **options):
        for r in Room.objects.all():
            try:
                collect_room_occupancy(r.pk)
            except Exception as e:
                logger.error(e)
