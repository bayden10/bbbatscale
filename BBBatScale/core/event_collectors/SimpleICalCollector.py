import recurring_ical_events

from core.event_collectors.base import EventCollectorStrategy
from urllib.request import urlopen as url_open
import icalendar
from django.utils.timezone import now
from datetime import timedelta
import logging
from core.models import Room, RoomEvent
from django.conf import settings

logger = logging.getLogger(__name__)


class SimpleICalCollector(EventCollectorStrategy):

    def __init__(self):
        self._necessary_keys = ["iCal_url", "iCal_encoding"]

    def collect_events(self, room_pk, parameters):
        logger.info("Start collect_events for room with pk={}".format(room_pk))

        parameters = self._evaluate_parameters(room_pk, parameters, self._necessary_keys)

        raw_calendar: str = url_open(parameters["iCal_url"]).read().decode(parameters["iCal_encoding"])

        events = self._extract_events_from_raw_calendar(raw_calendar)
        room_events = self._create_room_events_from_ical_events(events, room_pk)

        self._update_room_events_in_db(room_events, room_pk)

    def _extract_current_event_from_raw_calendar(self, raw_calendar):
        ical_calendar: icalendar.Calendar = icalendar.Calendar.from_ical(raw_calendar)
        unfolded_calendar = recurring_ical_events.of(ical_calendar)
        current_datetime = now()
        events = unfolded_calendar.between(current_datetime, current_datetime + timedelta(minutes=1))

        if len(events) > 0:
            return events[0]
        else:
            return None

    def _extract_events_from_raw_calendar(self, raw_calendar):
        ical_calendar: icalendar.Calendar = icalendar.Calendar.from_ical(raw_calendar)
        unfolded_calendar = recurring_ical_events.of(ical_calendar)
        current_datetime = now()
        events = unfolded_calendar.between(current_datetime,
                                           current_datetime +
                                           timedelta(hours=settings.EVENT_COLLECTION_SYNC_SYNC_HOURS))

        return events

    def _create_room_events_from_ical_events(self, ical_events, room_pk):
        return_events = {}
        room = Room.objects.get(pk=room_pk)

        for ical_event in ical_events:
            uid = ical_event.get("UID")
            name = ical_event.get("SUMMARY")
            start = ical_event.get("DTSTART").dt
            end = ical_event.get("DTEND").dt

            room_event = RoomEvent(uid=uid, name=name, room=room, start=start, end=end)

            return_events[uid] = room_event

        return return_events
