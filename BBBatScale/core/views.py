import csv
import json
import logging
from urllib.parse import unquote

from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse, HttpResponseNotAllowed
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import update_session_auth_hash


from core.constants import ROOM_STATE_ACTIVE, ROOM_STATE_INACTIVE, ROOM_STATE_CREATING
from core.filters import RoomFilter, InstanceFilter, TenantFilter, UserFilter
from core.models import Instance, Room, GeneralParameter, Tenant, RoomConfiguration, Meeting
#
from core.services import meeting_create, create_join_meeting_url, reset_room, translate_bbb_meta_data, room_click, \
    get_rooms_with_current_next_event
from core.utils import validate_json, validate_csv
from .forms import InstanceForm, RoomForm, RoomConfigurationForm, TenantForm, ConfigureRoomForm, RoomFilterFormHelper, \
    InstanceFilterFormHelper, TenantFilterFormHelper, UserForm, UserUpdateForm, PasswordChangeCrispyForm
from django.contrib.auth.models import User

logger = logging.getLogger(__name__)


def home(request):
    rooms = get_rooms_with_current_next_event()
    return render(request, 'home.html', {'instances': Instance.objects.all(),
                                         'rooms': rooms,
                                         'participants_current': Room.get_participants_current(),
                                         'performed_meetings': Meeting.objects.all().count(),
                                         'base_url': settings.BASE_URL
                                         })


def statistics(request):
    return render(request, 'statistics.html', {'instances': Instance.objects.all(),
                                               'rooms': Room.objects.all(),
                                               'participants_current': Room.get_participants_current(),
                                               'performed_meetings': Meeting.objects.all().count(),
                                               'tenants': Tenant.objects.all()})


@staff_member_required
def import_export(request):
    return render(request, 'import_export.html')


@staff_member_required
def import_upload_json(request):  # noqa: C901 TODO
    if request.method == 'POST':
        try:
            upload_file = request.FILES['document']
            file = json.load(upload_file)
            tenant_list = []
            tenant_updates = []
            instance_list = []
            instance_updates = []
            room_list = []
            room_updates = []

            uniq_constraints = validate_json(file)

            if any(uniq_constraints):
                return render(request, 'import_export.html', {'uniq_constraints': uniq_constraints})
            else:
                for i in range(len(file["tenant"])):
                    # create or update all tenants in json
                    _tenant, created = Tenant.objects.update_or_create(
                        name=file["tenant"][i]["name"],
                        defaults={
                            'description': file["tenant"][i]['description'],
                            'notifications_emails': file["tenant"][i]['notifications_emails']
                        }
                    )
                    tenant_list.append(_tenant.name) if created else tenant_updates.append(_tenant.name)

                    for instance in file["tenant"][i]["instances"]:
                        # create or update all instances belonging to current tenant
                        _instance, created = Instance.objects.update_or_create(
                            dns=instance['dns'],
                            defaults={
                                'tenant': _tenant,
                                'datacenter': instance['datacenter'],
                                'shared_secret': instance['shared_secret'],
                            }
                        )
                        instance_list.append(_instance.dns) if created else instance_updates.append(_instance.dns)

                    for room in file["tenant"][i]["rooms"]:
                        # create or update all rooms belonging to current tenant
                        _room, created = Room.objects.update_or_create(
                            name=room['name'],
                            defaults={
                                'tenant': _tenant,
                                'is_public': room['is_public'],
                                'comment_public': room['comment_public'],
                                'comment_private': room['comment_private'],
                            }
                        )
                        room_list.append(_room.name) if created else room_updates.append(_room.name)

                for i in range(len(file["instances"])):
                    _instance, created = Instance.objects.update_or_create(
                        dns=file["instances"][i]['dns'],
                        defaults={
                            'tenant': None,
                            'datacenter': file["instances"][i]['datacenter'],
                            'shared_secret': file["instances"][i]['shared_secret'],
                        }
                    )
                    instance_list.append(_instance.dns) if created else instance_updates.append(_instance.dns)

                for i in range(len(file["rooms"])):
                    # create or update all rooms belonging to current tenant
                    _room, created = Room.objects.update_or_create(
                        name=file["rooms"][i]['name'],
                        defaults={
                            'tenant': None,
                            'is_public': file["rooms"][i]['is_public'],
                            'comment_public': file["rooms"][i]['comment_public'],
                            'comment_private': file["rooms"][i]['comment_private'],
                        }
                    )
                    room_list.append(_room.name) if created else room_updates.append(_room.name)

            return render(request, 'import_export_importsuccess.html',
                          {'instance_list': instance_list, 'instance_updates': instance_updates,
                           'room_list': room_list,
                           'room_updates': room_updates, 'tenant_list': tenant_list,
                           'tenant_updates': tenant_updates})

        except KeyError:
            messages.error(request, _('KeyError - make sure all necessary attributes were set'))
            return redirect('import_export')

        except json.JSONDecodeError:
            messages.error(request, _('JSONDecodeError´- something must be wrong with your file structure'))
            return redirect('import_export')


@staff_member_required
def import_upload_csv(request):
    if request.method == 'POST':
        try:

            csv_file = request.FILES['document']
            room_list = []
            room_updates = []

            if not csv_file.name.endswith('.csv'):
                messages.error(request, 'This is not a csv file')

            decode_file = csv_file.read().decode('utf-8').splitlines()

            uniq_constraints = validate_csv(csv.DictReader(decode_file))

            if any(uniq_constraints):
                return render(request, 'import_export.html', {'uniq_constraints': uniq_constraints})
            else:
                for row in csv.DictReader(decode_file):
                    if Tenant.objects.filter(name=row['tenant']).count() >= 1:
                        tenant = Tenant.objects.get(name=row['tenant'])
                        _room, created = Room.objects.update_or_create(
                            name=row['name'],
                            defaults={
                                'tenant': tenant,
                                'is_public': row['is_public'],
                                'comment_public': row['comment_public'],
                                'comment_private': row['comment_private'],
                            }
                        )
                        room_list.append(_room.name) if created else room_updates.append(_room.name)
                    else:
                        _room, created = Room.objects.update_or_create(
                            name=row['name'],
                            defaults={
                                'tenant': None,
                                'is_public': row['is_public'],
                                'comment_public': row['comment_public'],
                                'comment_private': row['comment_private'],
                            }
                        )
                        room_list.append(_room.name) if created else room_updates.append(_room.name)

            return render(request, 'import_export_importsuccess.html',
                          {'room_list': room_list, 'room_updates': room_updates})
        except KeyError:
            messages.error(request, _('KeyError - make sure all necessary attributes were set'))
            return redirect('import_export')


@staff_member_required
def export_download_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="export.csv"'

    writer = csv.writer(response)

    writer.writerow(['tenant', 'name', 'is_public', 'comment_public', 'comment_private'])

    rooms = Room.objects.all().values_list('tenant__name', 'name', 'is_public', 'comment_public', 'comment_private')

    for room in rooms:
        writer.writerow(room)

    return response


@staff_member_required
def export_download_json(request):
    tenants = Tenant.objects.all()
    room_no_ten = Room.objects.filter(tenant__isnull=True)
    instance_no_ten = Instance.objects.filter(tenant__isnull=True)
    exportfile = {
        "tenant": [],
        "instances": [],
        "rooms": [],
    }

    for _tenant in tenants:
        # tenant -> instances/rooms
        tenant = {
            "name": _tenant.name,
            "description": _tenant.description,
            "notifications_emails": _tenant.notifications_emails,
            "instances": [],
            "rooms": [],
        }

        for _instance in _tenant.instance_set.all():
            # all instances that belong to current tenant
            instance = {
                "dns": _instance.dns,
                "datacenter": _instance.datacenter,
                "shared_secret": _instance.shared_secret,
            }
            tenant["instances"].append(instance)
        for _room in _tenant.room_set.all():
            # all rooms that belong to current tenant
            room = {
                "name": _room.name,
                "meeting_id": _room.meeting_id,
                "is_public": _room.is_public,
                "comment_public": _room.comment_public,
                "comment_private": _room.comment_private,
            }
            tenant["rooms"].append(room)
        exportfile['tenant'].append(tenant)

    for _instance in instance_no_ten:
        # all instances that belong to no tenant
        instance = {
            "dns": _instance.dns,
            "datacenter": _instance.datacenter,
            "shared_secret": _instance.shared_secret,
        }
        exportfile['instances'].append(instance)
    for _room in room_no_ten:
        # all rooms that belong to no tenant
        room = {
            "name": _room.name,
            "meeting_id": _room.meeting_id,
            "is_public": _room.is_public,
            "comment_public": _room.comment_public,
            "comment_private": _room.comment_private,
        }
        exportfile['rooms'].append(room)

    result = json.dumps(exportfile, indent=4)

    response = HttpResponse(result, content_type='application/json')
    response['Content-Disposition'] = 'attachment; filename="export.json"'

    return response


def redirect_two_params(request, building, room):
    name = "{}/{}".format(building, room)
    try:
        _room = Room.objects.get(name=name)
        room_click(_room.pk)
        return redirect('join_or_create_meeting', _room.meeting_id)
    except Room.DoesNotExist:
        messages.error(request, _("Unable to find Room with given name: '{}'. ".format(name)))
        return redirect('home')


def redirect_one_param(request, param):
    name = unquote(param)
    try:
        _room = Room.objects.get(name=name)
        room_click(_room.pk)
        return redirect('join_or_create_meeting', _room.meeting_id)
    except Room.DoesNotExist:
        messages.error(request, _("Unable to find Room with given name: '{}'. ".format(name)))
        return redirect('home')


@staff_member_required
def instances_overview(request):
    f = InstanceFilter(request.GET, queryset=Instance.objects.all())
    f.form.helper = InstanceFilterFormHelper
    return render(request, 'instances_overview.html', {'filter': f})


@staff_member_required
def instance_create(request):
    form = InstanceForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        instance = form.save(commit=False)

        # Custom form validations

        instance.save()

        messages.success(request, _("Server: {} was created successfully.".format(instance.dns)))
        # return to URL
        return redirect('instances_overview')
    return render(request, 'instances_create.html', {'form': form})


@staff_member_required
def instance_delete(request, instance):
    instance = get_object_or_404(Instance, pk=instance)
    instance.delete()
    messages.success(request, _("Server was deleted successfully"))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@staff_member_required
def instance_update(request, instance):
    instance = get_object_or_404(Instance, pk=instance)
    form = InstanceForm(request.POST or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            _instance = form.save(commit=False)

            _instance.save()

            messages.success(request, _("Server: {} was updated successfully.".format(_instance.dns)))
            return redirect('instances_overview')
    return render(request, 'instances_create.html', {'form': form})


@staff_member_required
def rooms_overview(request):
    f = RoomFilter(request.GET, queryset=Room.objects.all())
    f.form.helper = RoomFilterFormHelper
    return render(request, 'rooms_overview.html', {'filter': f})


@staff_member_required
def room_create(request):
    form = RoomForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        # Custom form validations
        if _room.config:
            _room.mute_on_start = _room.config.mute_on_start
            _room.all_moderator = _room.config.all_moderator
            _room.everyone_can_start = _room.config.everyone_can_start
            _room.authenticated_user_can_start = _room.config.authenticated_user_can_start
            _room.guest_policy = _room.config.guest_policy
            _room.allow_guest_entry = _room.config.allow_guest_entry
            _room.access_code = _room.config.access_code
            _room.access_code_guests = _room.config.access_code_guests
            _room.disable_cam = _room.config.disable_cam
            _room.disable_mic = _room.config.disable_mic
            _room.allow_recording = _room.config.allow_recording
            _room.url = _room.config.url
        _room.save()

        messages.success(request, _("Room {} was created successfully.".format(_room.name)))
        # return to URL
        return redirect('rooms_overview')
    return render(request, 'rooms_create.html', {'form': form})


@staff_member_required
def room_details(request, room):
    return render(request, '', {'room': Room.objects.get(pk=room)})


@staff_member_required
def room_delete(request, room):
    instance = get_object_or_404(Room, pk=room)
    instance.delete()
    messages.success(request, _("Room was deleted successfully."))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@staff_member_required
def room_update(request, room):
    instance = get_object_or_404(Room, pk=room)
    form = RoomForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        if _room.config:
            _room.mute_on_start = _room.config.mute_on_start
            _room.all_moderator = _room.config.all_moderator
            _room.everyone_can_start = _room.config.everyone_can_start
            _room.authenticated_user_can_start = _room.config.authenticated_user_can_start
            _room.guest_policy = _room.config.guest_policy
            _room.allow_guest_entry = _room.config.allow_guest_entry
            _room.access_code = _room.config.access_code
            _room.access_code_guests = _room.config.access_code_guests
            _room.disable_cam = _room.config.disable_cam
            _room.disable_mic = _room.config.disable_mic
            _room.allow_recording = _room.config.allow_recording
            _room.url = _room.config.url
        _room.save()
        messages.success(request, _("Room {} was updated successfully.".format(_room.name)))
        return redirect('rooms_overview')
    return render(request, 'rooms_create.html', {'form': form})


def room_config_overview(request):
    context = {
        "configs": RoomConfiguration.objects.all()
    }
    return render(request, "room_configs_overview.html", context)


@staff_member_required
def room_config_create(request):
    form = RoomConfigurationForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        room_config = form.save(commit=False)

        # Custom form validations
        if "CustomConfig" in request.user.groups.all():
            room_config.user = request.user
            room_config.name = "{}-{}".format(request.user.username, room_config.name)
        room_config.save()

        messages.success(request, _("Room configuration was created successfully."))
        # return to URL
        return redirect('room_configs_overview')
    return render(request, 'room_configs_create.html', {'form': form})


@staff_member_required
def room_config_delete(request, room_config):
    instance = get_object_or_404(RoomConfiguration, pk=room_config)
    instance.delete()
    messages.success(request, _("Room configuration was deleted successfully."))
    return redirect('room_configs_overview')


@staff_member_required
def room_config_update(request, room_config):
    instance = get_object_or_404(RoomConfiguration, pk=room_config)
    form = RoomConfigurationForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _room_config = form.save(commit=False)
        _room_config.save()
        _room_config.room_set.filter(state=ROOM_STATE_INACTIVE).update(
            mute_on_start=_room_config.mute_on_start,
            all_moderator=_room_config.all_moderator,
            everyone_can_start=_room_config.everyone_can_start,
            authenticated_user_can_start=_room_config.authenticated_user_can_start,
            guest_policy=_room_config.guest_policy,
            allow_guest_entry=_room_config.allow_guest_entry,
            access_code=_room_config.access_code,
            access_code_guests=_room_config.access_code_guests,
            disable_cam=_room_config.disable_cam,
            disable_mic=_room_config.disable_mic,
            allow_recording=_room_config.allow_recording,
            url=_room_config.url,
        )
        messages.success(request, _("Room configuration was updated successfully."))
        return redirect('room_configs_overview')
    return render(request, 'room_configs_create.html', {'form': form})


@staff_member_required
def tenant_overview(request):
    f = TenantFilter(request.GET, queryset=Tenant.objects.all())
    f.form.helper = TenantFilterFormHelper
    return render(request, "tenants_overview.html", context={"filter": f})


@staff_member_required
def tenant_create(request):
    form = TenantForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        tenant = form.save(commit=False)

        # Custom form validations

        tenant.save()

        messages.success(request, _("Tenant was created successfully."))
        # return to URL
        return redirect('tenants_overview')
    return render(request, 'tenants_create.html', {'form': form})


@staff_member_required
def tenant_delete(request, tenant):
    instance = get_object_or_404(Tenant, pk=tenant)
    instance.delete()
    messages.success(request, _("Tenant was deleted successfully."))
    return redirect('tenants_overview')


@staff_member_required
def tenant_update(request, tenant):
    instance = get_object_or_404(Tenant, pk=tenant)
    form = TenantForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _tenant = form.save(commit=False)

        _tenant.save()

        messages.success(request, _("Tenant was updated successfully."))
        return redirect('tenants_overview')
    return render(request, 'tenants_create.html', {'form': form})


def session_set_username(request):
    if not request.is_ajax() or not request.method == "POST":
        return HttpResponseNotAllowed(['POST'])
    request.session['username'] = request.POST.get("username")
    return HttpResponse('ok')


def join_or_create_meeting(request, room):
    _room = get_object_or_404(Room, meeting_id=room)
    context = {}
    context.update({
        "room": _room
    })
    if not request.user.is_authenticated and _room.allow_guest_entry and "username" not in request.session:
        context.update({
            "user_name_required": True
        })
        return render(request, "join_meeting.html", context)
    elif not request.user.is_authenticated and not _room.allow_guest_entry:
        messages.warning(request,
                         _("The meeting is not allowed for guest entry. Please login to access the meeting."))
        response = redirect('login')
        response['Location'] += '?next={}'.format(request.path_info)
        return response
    if (_room.everyone_can_start and _room.state == ROOM_STATE_INACTIVE) or \
            ((_room.authenticated_user_can_start or request.user.groups.filter(
                name="Teacher").exists() or request.user.is_superuser or request.user.is_staff) and
             (_room.state == ROOM_STATE_INACTIVE and request.user.is_authenticated)):
        return redirect('create_meeting', _room.meeting_id)
    return redirect('join_meeting', _room.meeting_id)


def create_meeting(request, room):
    # Query Database Object and lock it for feature requests!
    instance = get_object_or_404(Room, meeting_id=room)
    form = ConfigureRoomForm(request.POST or None, instance=instance)
    if "username" in request.session:
        join_name = request.session['username']
    else:
        join_name = "{}, {}".format(request.user.last_name, request.user.first_name)
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)

        if Room.objects.filter(meeting_id=room, state=ROOM_STATE_INACTIVE).update(state=ROOM_STATE_CREATING,
                                                                                  participant_count=1) >= 1:
            context = {
                'name': instance.name,
                'meeting_id': instance.meeting_id,
                'attendee_pw': instance.attendee_pw,
                'moderator_pw': instance.moderator_pw,
                'mute_on_start': _room.mute_on_start,
                'all_moderator': _room.all_moderator,
                'everyone_can_start': _room.everyone_can_start,
                'access_code': _room.access_code,
                'access_code_guests': _room.access_code_guests,
                'disable_cam': _room.disable_cam,
                'disable_mic': _room.disable_mic,
                'disable_note': _room.disable_note,
                'disable_private_chat': _room.disable_private_chat,
                'disable_public_chat': _room.disable_public_chat,
                'allow_recording': _room.allow_recording,
                'guest_policy': _room.guest_policy,
                'url': _room.url,
                'creator': join_name,
                'allow_guest_entry': _room.allow_guest_entry,
            }
            meeting_create(context)
            return redirect('join_meeting', _room.meeting_id)
        else:
            messages.warning(request,
                             _(
                                 "The meeting was started by another moderator." +
                                 " You will be redirected to the meeting shortly."))
            return redirect('join_or_create_meeting', instance.meeting_id)
    return render(request, 'configure_room_for_meeting.html', {'form': form, 'room': instance})


def join_meeting(request, room):  # noqa: C901 TODO
    _room = get_object_or_404(Room, meeting_id=room)
    context = {}
    context.update({
        "room": _room
    })
    # User not logged in and meeting not public redirect to frontpage with message
    if _room.allow_guest_entry and not request.user.is_authenticated:
        if "username" in request.session:
            username = request.session['username']
        else:
            return redirect('join_or_create_meeting', _room.meeting_id)
    elif not request.user.is_authenticated:
        messages.warning(request,
                         _("The moderator did not allow guest entry. Please login to access the meeting. "))
        response = redirect('login')
        response['Location'] += '?next={}'.format(request.path_info)
        return response
    # Check if meeting is running
    if _room.state == ROOM_STATE_ACTIVE:
        # Set variables to start or join meeting
        join_name = "{}, {}".format(request.user.last_name,
                                    request.user.first_name) if request.user.is_authenticated else username
        password = _room.moderator_pw if _room.all_moderator or request.user.groups.filter(
            name="Teacher").exists() or request.user.is_superuser or request.user.is_staff else _room.attendee_pw
        # check if guests are allowed
        if not _room.access_code:
            if _room.allow_guest_entry and not request.user.is_authenticated:
                # Check if guest access code is set
                if not _room.access_code_guests:
                    try:
                        del request.session['username']
                    except KeyError:
                        pass
                    return redirect(create_join_meeting_url(_room.meeting_id, join_name, password))
                else:
                    if request.method == "POST" and request.GET.get('action') == "guest_access_code":
                        ac = request.POST.get('access_code')
                        if ac and ac == _room.access_code_guests:
                            try:
                                del request.session['username']
                            except KeyError:
                                pass
                            return redirect(create_join_meeting_url(_room.meeting_id, join_name, password))
                        else:
                            messages.error(request, _("Incorrect guest access code."))
                    context.update({
                        "guest_access_code": True
                    })
                    return render(request, "join_meeting.html", context)
            elif not _room.allow_guest_entry and not request.user.is_authenticated:
                messages.warning(request,
                                 _("The moderator did not allow guest entry. Please login to access the meeting. "))
                response = redirect('login')
                response['Location'] += '?next={}'.format(request.path_info)
                return response
            else:
                try:
                    del request.session['username']
                except KeyError:
                    pass
                return redirect(create_join_meeting_url(_room.meeting_id, join_name, password))
        else:
            if request.method == "POST" and request.GET.get('action') == "general_access_code":
                ac = request.POST.get('access_code')
                if ac and ac == _room.access_code:
                    if not _room.allow_guest_entry and not request.user.is_authenticated:
                        messages.warning(request,
                                         _(
                                             "The moderator did not allow guest entry." +
                                             " Please login to access the meeting. "))
                        response = redirect('login')
                        response['Location'] += '?next={}'.format(request.path_info)
                        return response
                    try:
                        del request.session['username']
                    except KeyError:
                        pass
                    return redirect(create_join_meeting_url(_room.meeting_id, join_name, password))
                else:
                    messages.error(request, _("Incorrect access code."))
            context.update({
                "access_code": True
            })
            # Meeting is not running or access code is needed
    return render(request, "join_meeting.html", context)


def get_meeting_status(request, meeting_id):
    _room = get_object_or_404(Room, meeting_id=meeting_id)
    if _room.state == ROOM_STATE_ACTIVE:
        logger.info("Was asked for state of {}. Answer is {}".format(_room, _room.state))
        return JsonResponse({"is_running": True})
    logger.info("Was asked for state of {}. Answer is {}".format(_room, _room.state))
    return JsonResponse({"is_running": False})


@csrf_exempt
def callback_bbb(request):
    logger.info("Start processing bbb callback request...")
    logger.info(request.POST['event'])

    token = request.META.get('HTTP_AUTHORIZATION').split(" ")[1]
    j = json.loads(request.POST['event'])
    domain = request.POST['domain']
    event = j[0]['data']['id']
    i = Instance.objects.get(dns=domain)
    if token == i.shared_secret:
        logger.info("Token validation passed.")

        if event in ['meeting-created', 'meeting-ended']:
            meeting_id = j[0]['data']['attributes']['meeting']['external-meeting-id']
            logger.info(
                "event {} for meeting (id={}, domain={})".format(event, meeting_id, domain))
            if event == "meeting-created":
                breakout = j[0]['data']['attributes']['meeting']['is-breakout']
                name = j[0]['data']['attributes']['meeting']['name']
                meta = translate_bbb_meta_data(j[0]['data']['attributes']['meeting']['metadata'])
                logger.info(
                    "meeting created (domain={}, id={}, room-name={})".format(domain, name,
                                                                              meeting_id))
                meta.update({"name": name, "instance": i, "tenant": i.tenant, "last_running": now(),
                             "state": ROOM_STATE_ACTIVE, "is_breakout": breakout})
                room, created = Room.objects.update_or_create(
                    meeting_id=meeting_id,
                    defaults=meta
                )
                if created:
                    logger.info(
                        "room created (name={}, instance={}, tenant={}, breakout={}, )".format(room.name,
                                                                                               room.instance.dns,
                                                                                               room.tenant.name,
                                                                                               room.is_breakout))
                else:
                    logger.info(
                        "room updated (name={}, instance={}, tenant={},breakout={}, )".format(room.name,
                                                                                              room.instance.dns,
                                                                                              room.tenant.name,
                                                                                              room.is_breakout))
            if event == "meeting-ended":
                room = Room.objects.get(meeting_id=meeting_id)
                logger.info(
                    "meeting ended (domain={}, id={}, room-name={})".format(domain, room.name,
                                                                            room.meeting_id))
                if room.is_breakout:
                    logger.info(
                        "meeting is breakout will be deleted(domain={}, id={}, room-name={})".format(domain, room.name,
                                                                                                     room.meeting_id))
                    room.delete()
                else:
                    logger.info(
                        "meeting is not breakout will be reset(domain={}, id={}, room-name={})".format(domain,
                                                                                                       room.name,
                                                                                                       room.meeting_id))
                    reset_room(room.meeting_id, room.name, room.config)

    logger.info("Finished processing bbb callback request")
    return JsonResponse({"status": "ok"})


@csrf_exempt
def recording_callback(request):
    data = json.loads(request.body)
    if data['token'] == settings.RECORDINGS_SECRET:
        try:
            Meeting.objects.filter(pk=data['rooms_meeting_id']).update(replay_id=data['bbb_meeting_id'])
            return HttpResponse(status=200)
        except Meeting.DoesNotExist:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=401)


@csrf_exempt
def api_instance_registration(request):
    data = json.loads(request.body)
    try:
        t = Tenant.objects.get(token_registration=data['token'])
        Instance.objects.get_or_create(dns=data['instance'], shared_secret=data['secret'], tenant=t)
        return HttpResponse(status=200)
    except Tenant.DoesNotExist:
        return HttpResponse(status=401)


def recordings_list(request):
    context = {}
    if request.user.is_staff:
        context.update({"recordings": Meeting.objects.filter(replay_id__isnull=False)})
    else:
        context.update({"recordings": Meeting.objects.filter(
            creator="{}, {}".format(request.user.last_name, request.user.first_name),
            replay_id__isnull=False)})
    context.update({"base_url": settings.BASE_URL})
    return render(request, "recordings_overview.html", context)


def recording_redirect(request, replay_id):
    if request.user.is_authenticated:
        meeting = get_object_or_404(Meeting, replay_id=replay_id)
        gp = GeneralParameter.objects.first()
        return redirect(gp.playback_url + meeting.replay_id)
    else:
        messages.warning(request,
                         _("The meeting is not allowed for guest entry. Please login to access the meeting."))
        response = redirect('login')
        response['Location'] += '?next={}'.format(request.path_info)
        return response


@staff_member_required
def force_end_meeting(request, room_pk):
    room = get_object_or_404(Room, pk=room_pk)
    if room.end_meeting():
        messages.success(request, _("Meeting in {} successfully ended.".format(room.name)))
    else:
        messages.error(request, _("Error while trying to end meeting in {}.".format(room.name)))
    return redirect('rooms_overview')


@staff_member_required
def users_overview(request):
    f = UserFilter(request.GET, queryset=User.objects.all())
    f.form.helper = TenantFilterFormHelper
    return render(request, 'users_overview.html', {'filter': f})


@staff_member_required
def user_create(request):
    form = UserForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        user = form.save(commit=False)
        user.set_password(user.password)
        # Custom form validations

        user.save()
        for group in form.cleaned_data.get('groups'):
            user.groups.add(group)
        messages.success(request, _("User {} successfully created.".format(user.username)))
        # return to URL
        return redirect('users_overview')
    return render(request, 'user_create.html', {'form': form})


@staff_member_required
def user_delete(request, user):
    instance = get_object_or_404(User, pk=user)
    instance.delete()
    messages.success(request, _("User successfully deleted."))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@staff_member_required
def user_update(request, user):
    instance = get_object_or_404(User, pk=user)

    if request.method == "POST":
        form = UserUpdateForm(request.POST, instance=instance)
        if form.is_valid():
            _user = form.save(commit=False)

            _user.save()
            _user.groups.clear()
            for group in form.cleaned_data.get('groups'):
                _user.groups.add(group)

            messages.success(request, _("User {} successfully updated".format(_user.username)))
            return redirect('users_overview')
    form = UserUpdateForm(instance=instance, initial={'groups': instance.groups.all()})
    return render(request, 'user_create.html', {'form': form})


def change_password(request):
    form = PasswordChangeCrispyForm(request.user, request.POST or None)
    if request.method == "POST" and form.is_valid():
        user = form.save()
        update_session_auth_hash(request, user)
        messages.success(request, _("Password successfully changed."))
        return redirect('home')
    if "BBBatScale.authbackends.LDAPBackend" in settings.AUTHENTICATION_BACKENDS:
        messages.warning(request, _("Password changing has no effect when LDAP is enabled."))
    return render(request, 'password_change.html', {'form': form, })
